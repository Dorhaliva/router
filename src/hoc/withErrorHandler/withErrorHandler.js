import React, { Component, Fragment } from 'react';

import Modal from '../../componentes/UI/Modal/Modal';

const withErrorHandler = (WrappedComponent, axios) => {
	return class extends Component {
		constructor(props) {
			super(props);
			this.reqInter = axios.interceptors.request.use(req => {
				this.setState({ error: null });
				return req;
			});
			this.resInter = axios.interceptors.response.use(
				res => res,
				error => {
					this.setState({ error: error });
				}
			);
		}
		state = {
			error: null
		};

		componentWillUnmount() {
			axios.interceptors.request.eject(this.reqInter);
			axios.interceptors.response.eject(this.response);
		}

		errorConfirmedHandler = () => {
			this.setState({ error: null });
		};

		render() {
			return (
				<Fragment>
					<Modal
						show={this.state.error}
						modalClosed={this.errorConfirmedHandler}
					>
						{this.state.error ? this.state.error.message : null}
					</Modal>
					<WrappedComponent {...this.props} />
				</Fragment>
			);
		}
	};
};

export default withErrorHandler;
