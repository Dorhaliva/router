import React, { Fragment, Component } from 'react';
import Toolbar from '../../componentes/Navigation/Toolbar/Toolbar';
import { connect } from 'react-redux';
import classes from './Layout.css';
import SideDrawer from '../../componentes/Navigation/SideDrawer/SideDrawer';

class Layout extends Component {
	state = {
		showSideDrawer: false
	};

	SideDrawerCloseHandler = () => {
		this.setState({ showSideDrawer: false });
	};

	sideDrawerOpenHandler = () => {
		this.setState({ showSideDrawer: true });
	};

	render() {
		return (
			<Fragment>
				<Toolbar
					isAuth={this.props.isAuth}
					open={this.sideDrawerOpenHandler}
				></Toolbar>
				<SideDrawer
					isAuth={this.props.isAuth}
					isOpen={this.state.showSideDrawer}
					closed={this.SideDrawerCloseHandler}
				></SideDrawer>
				<main className={classes.Content}>{this.props.children}</main>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuth: state.auth.idToken !== null
	};
};

export default connect(mapStateToProps)(Layout);
