import axios from 'axios';

const instance = axios.create({
	baseURL: 'https://react-my-burger-2de2b.firebaseio.com/'
});

export default instance;
