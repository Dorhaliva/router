import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import './App.css';
import Layout from './hoc/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Checkout from './containers/Checkout/Checkout';
import Orders from './containers/Orders/Orders';
import Auth from './containers/Auth/Auth';
import Logout from './containers/Auth/Logout/Logout';
import { connect } from 'react-redux';

import { authCheckState } from './store/actions/index';

class App extends Component {
	componentDidMount() {
		this.props.onTryAutoSignUp();
	}

	render() {
		let routes = (
			<Switch>
				<Route path="/auth" component={Auth}></Route>

				<Route path="/" exact component={BurgerBuilder}></Route>
				<Redirect to="/"></Redirect>
			</Switch>
		);

		if (this.props.isAuth) {
			routes = (
				<Switch>
					<Route path="/orders" component={Orders}></Route>

					<Route path="/checkout" component={Checkout}></Route>
					<Route path="/logout" component={Logout}></Route>

					<Route path="/" exact component={BurgerBuilder}></Route>
					<Redirect to="/"></Redirect>
				</Switch>
			);
		}
		return (
			<div>
				<BrowserRouter>
					<Layout>{routes}</Layout>
				</BrowserRouter>
			</div>
		);
	}
}
const mapStateToProps = state => {
	return {
		isAuth: state.auth.idToken !== null
	};
};
const mapDispatchToProps = dispatch => {
	return {
		onTryAutoSignUp: () => dispatch(authCheckState())
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
