import React, { Component } from 'react';
import Order from '../../componentes/Order/Order';
import Spinner from '../../componentes/UI/Spinner/Spinner';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
class Orders extends Component {
	componentDidMount() {
		this.props.getOrders(this.props.token, this.props.userId);
	}

	render() {
		let orders = <Spinner></Spinner>;

		if (!this.props.loading)
			if (this.props.orders.length === 0) {
				orders = <p>No orders</p>;
			} else {
				orders = Object.keys(this.props.orders).map(orderKey => {
					return (
						<Order
							key={orderKey}
							ingredients={
								this.props.orders[orderKey].ingredients
							}
							price={this.props.orders[orderKey].price}
						></Order>
					);
				});
			}
		return <div>{orders}</div>;
	}
}

const mapStateToProps = state => {
	return {
		orders: state.order.orders,
		loading: state.order.loading,
		userId: state.auth.userId
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getOrders: token => dispatch(actions.getOrders(token))
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withErrorHandler(Orders, axios));
