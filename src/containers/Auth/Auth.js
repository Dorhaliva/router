import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from '../../componentes/UI/Input/Input';
import Button from '../../componentes/UI/Button/Button';
import * as actions from '../../store/actions/auth';
import classes from './Auth.css';
import Spinner from '../../componentes/UI/Spinner/Spinner';
import { Redirect } from 'react-router';
import { updateObject, checkValidity } from '../../shared/utillity';
class Auth extends Component {
	state = {
		controls: {
			email: {
				elementType: 'input',
				elementConfig: {
					type: 'email',
					placeholder: 'Mail Address'
				},
				value: '',
				validation: {
					required: true,
					isEmail: true
				},
				valid: false,
				touched: false
			},
			password: {
				elementType: 'input',
				elementConfig: {
					type: 'password',
					placeholder: 'password'
				},
				value: '',
				validation: {
					required: true,
					minLength: 6
				},
				valid: false,
				touched: false
			}
		},
		isSignUp: true
	};

	inputChangedHandler = (event, controlName) => {
		const updateControls = updateObject(this.state.controls, {
			[controlName]: updateObject(this.state.controls[controlName], {
				value: event.target.value,
				valid: checkValidity(
					event.target.value,
					this.state.controls[controlName].validation
				),
				touched: true
			})
		});
		this.setState({ controls: updateControls });
	};

	submitHamdler = event => {
		event.preventDefault();
		this.props.onAuth(
			this.state.controls.email.value,
			this.state.controls.password.value,
			this.state.isSignUp
		);
	};
	switchAuthModeHandler = () => {
		this.setState(prevState => {
			return { isSignUp: !prevState.isSignUp };
		});
	};

	render() {
		let inputs = [];
		for (let key in this.state.controls) {
			inputs.push({
				id: key,
				config: this.state.controls[key]
			});
		}

		let form = inputs.map(inputElement => (
			<Input
				key={inputElement.id}
				click={event =>
					this.inputChangedHandler(event, inputElement.id)
				}
				elementType={inputElement.config.elementType}
				elementConfig={inputElement.config.elementConfig}
				value={inputElement.config.value}
				label={inputElement.id}
				shouldValidate={
					inputElement.config.validation &&
					inputElement.config.touched
				}
				valid={inputElement.config.valid}
			/>
		));

		if (this.props.loading) {
			form = <Spinner />;
		}

		let authRedirect = null;
		if (this.props.isAuth) {
			authRedirect = <Redirect to="/" />;
		}
		return (
			<div className={classes.Auth}>
				{authRedirect}
				<p> {this.props.error} </p>
				<form onSubmit={this.submitHamdler}>
					{form}
					<Button btnType="Success">Submit</Button>
				</form>
				<Button btnType="Danger" clicked={this.switchAuthModeHandler}>
					SWITCH TO {this.state.isSignUp ? 'SIGNIN' : 'SIGNUP'}{' '}
				</Button>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		loading: state.auth.loading,
		error: state.auth.error,
		isAuth: state.auth.idToken !== null,
		authRedirectPath: state.auth.authRedirectPath
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onAuth: (email, password, isSignUp) =>
			dispatch(actions.auth(email, password, isSignUp))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
