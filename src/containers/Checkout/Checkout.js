import React, { Component, Fragment } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import CheckoutSummary from '../../componentes/Order/CheckoutSummary/CheckoutSummary';
import ContantData from '../Checkout/ContentData/ContentData';
class Checkout extends Component {
	cancelCheckout = () => {
		this.props.history.goBack();
	};

	continueCheckout = () => {
		this.props.history.replace('/checkout/contant-data');
	};

	render() {
		let checkoutSummary = <Redirect to="/" />;
		if (this.props.ingredients) {
			let redirectOrder = null;
			if (this.props.purchased) {
				redirectOrder = <Redirect to="/"> </Redirect>;
			}
			checkoutSummary = (
				<Fragment>
					{redirectOrder}
					<CheckoutSummary
						ingredients={this.props.ingredients}
						cancelClick={this.cancelCheckout}
						continueClick={this.continueCheckout}
					/>
					<Route
						path={this.props.match.path + '/contant-data'}
						component={ContantData}
					/>
				</Fragment>
			);
		}

		return checkoutSummary;
	}
}

const mapStateToProps = state => {
	return {
		ingredients: state.burger.ingredients,
		purchased: state.order.purchased
	};
};

export default connect(mapStateToProps)(Checkout);
