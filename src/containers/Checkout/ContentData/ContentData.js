import React, { Component } from 'react';
import Button from '../../../componentes/UI/Button/Button';
import classes from './ContentData.css';
import axios from '../../../axios-orders';
import Spinner from '../../../componentes/UI/Spinner/Spinner';
import Input from '../../../componentes/UI/Input/Input';
import { connect } from 'react-redux';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../../store/actions/index';
import { updateObject, checkValidity } from '../../../shared/utillity';
class ContantData extends Component {
	state = {
		orderForm: {
			name: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Your Name'
				},
				value: '',
				validation: {
					required: true
				},
				valid: false,
				touched: false
			},
			street: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Your Street'
				},
				value: '',
				validation: {
					required: true
				},
				valid: false,
				touched: false
			},
			email: {
				elementType: 'input',
				elementConfig: {
					type: 'email',
					placeholder: 'Your E-mail'
				},
				value: '',
				validation: {
					required: true
				},
				valid: false,
				touched: false
			},
			zipCode: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Your Zip Code'
				},
				value: '',
				validation: {
					required: true
				},
				valid: false,
				touched: false
			},
			country: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Your country'
				},
				value: '',
				validation: {
					required: true
				},
				valid: false,
				touched: false
			},
			deliveryMethod: {
				elementType: 'select',
				elementConfig: {
					options: [
						{ value: 'fastest', displayValue: 'Fastest' },
						{ value: 'cheapest', displayValue: 'Cheapest' }
					]
				},
				valid: true,
				validation: {},
				value: 'fastest'
			}
		},
		isFormValid: false
	};

	orderHandler = event => {
		event.preventDefault();
		const formData = {};
		for (let key in this.state.orderForm) {
			formData[key] = this.state.orderForm[key].value;
		}

		const order = {
			ingredients: this.props.ingredients,
			price: this.props.price,
			orderData: formData,
			userId: this.props.userId
		};

		this.props.onOrder(order, this.props.token);
	};

	inputChangedHandler = (event, key) => {
		const updateFormElement = updateObject(
			this.state.updateFormElement[key],
			{
				value: event.target.value,
				valid: checkValidity(
					event.target.value,
					this.state.updateFormElement[key].validation
				),
				touched: true
			}
		);

		const updateOrderForm = updateObject(this.state.orderForm, {
			[key]: updateFormElement
		});

		this.setState({
			orderForm: updateOrderForm,
			isFormValid: this.checkFormValidation(updateOrderForm)
		});
	};

	checkFormValidation(updateOrderForm) {
		let formIsValid = true;
		for (let key in updateOrderForm.orderForm) {
			formIsValid = updateOrderForm[key].valid && formIsValid;
		}
		return formIsValid;
	}
	render() {
		let inputs = [];
		for (let key in this.state.orderForm) {
			inputs.push({
				id: key,
				config: this.state.orderForm[key]
			});
		}

		let form = <Spinner />;
		if (!this.props.loading) {
			form = (
				<form onSubmit={this.orderHandler}>
					{inputs.map(inputElement => (
						<Input
							key={inputElement.id}
							click={event =>
								this.inputChangedHandler(event, inputElement.id)
							}
							elementType={inputElement.config.elementType}
							elementConfig={inputElement.config.elementConfig}
							value={inputElement.config.value}
							label={inputElement.id}
							shouldValidate={
								inputElement.config.validation &&
								inputElement.config.touched
							}
							valid={inputElement.config.valid}
						/>
					))}
					<Button
						btnType="Success"
						disabled={!this.state.isFormValid}
					>
						Order
					</Button>
				</form>
			);
		}
		return (
			<div className={classes.ContentData}>
				<h4> Enter Your Contant Data</h4>
				{form}
			</div>
		);
	}
}
const mapStateToProps = state => {
	return {
		ingredients: state.burger.ingredients,
		price: state.burger.totalPrice,
		loading: state.order.loading,
		token: state.auth.idToken,
		userId: state.auth.userId
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onOrder: (orderData, token) =>
			dispatch(actions.purcahseBurger(orderData, token))
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withErrorHandler(ContantData, axios));
