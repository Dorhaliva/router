import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
import Burger from '../../componentes/Burger/Burger';
import BuildControls from '../../componentes/Burger/BuildControls/BuildControls';
import Modal from '../../componentes/UI/Modal/Modal';
import axios from '../../axios-orders';
import classes from './BurgerBuilder.css';
import OrderSummary from '../../componentes/Burger/OrderSummary/OrderSummary';
import Spinner from '../../componentes/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';

export class BurgerBuilder extends Component {
	state = {
		totalPrice: 4,
		purchasing: false
	};

	componentDidMount() {
		this.props.onInitIngredients();
	}

	updatePurchasable = ingredients => {
		const sum = Object.keys(ingredients)
			.map(igKey => {
				return ingredients[igKey];
			})
			.reduce((sum, el) => {
				return sum + el;
			}, 0);
		return sum > 0;
	};

	purchaseHandler = () => {
		if (this.props.isAuth) {
			this.setState({ purchasing: true });
		} else {
			this.props.history.push('/auth');
		}
	};

	purchaseCancelHandler = () => {
		this.setState({ purchasing: false });
	};

	purchaseContinueHandler = () => {
		this.props.onInitPurchase();
		this.props.history.push('/checkout');
	};

	render() {
		const disabledInfo = {
			...this.props.ingredients
		};

		for (let key in disabledInfo) {
			disabledInfo[key] = disabledInfo[key] === 0;
		}

		let orderSummary = <Spinner />;

		let burger = <Spinner />;
		if (this.props.error) {
			burger = <p> Burger can't be loaded</p>;
		} else if (this.props.ingredients) {
			burger = (
				<Fragment>
					<Burger ingredients={this.props.ingredients}></Burger>
					<p className={classes.Price}>
						Current Price: {this.props.totalPrice.toFixed(2)}
					</p>

					<BuildControls
						isAuth={this.props.isAuth}
						add={this.props.onIngredientAdded}
						remove={this.props.onIngredientRemove}
						disabled={disabledInfo}
						ordered={this.purchaseHandler}
						purchasable={this.updatePurchasable(
							this.props.ingredients
						)}
					/>
				</Fragment>
			);

			orderSummary = (
				<OrderSummary
					price={this.props.totalPrice}
					cancel={this.purchaseCancelHandler}
					continue={this.purchaseContinueHandler}
					ingredients={this.props.ingredients}
				/>
			);
		}

		return (
			<Fragment>
				<Modal
					show={this.state.purchasing}
					modalClosed={this.purchaseCancelHandler}
				>
					{orderSummary}
				</Modal>
				{burger}
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		ingredients: state.burger.ingredients,
		totalPrice: state.burger.totalPrice,
		error: state.burger.error,
		isAuth: state.auth.idToken !== null
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onIngredientAdded: ingredientName =>
			dispatch(actions.addIngredient(ingredientName)),
		onIngredientRemove: ingredientName =>
			dispatch(actions.removeIngredient(ingredientName)),
		onInitIngredients: () => dispatch(actions.initIngredients()),
		onInitPurchase: () => dispatch(actions.purchaseInit())
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withErrorHandler(BurgerBuilder, axios));
