import { BurgerBuilder } from './BurgerBuilder';
import BuildControls from '../../componentes/Burger/BuildControls/BuildControls';
import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('<BurgerBuilder />', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallow(
			<BurgerBuilder onInitIngredients={() => {}} totalPrice={0} />
		);
	});

	it('should render BuildControls', () => {
		wrapper.setProps({ ingredients: { Salad: 0 } });
		expect(wrapper.find(BuildControls)).toHaveLength(1);
	});
});
