import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utillity';

const initialState = {
	ingredients: null,
	totalPrice: 4,
	error: false
};

const INGREDIENTS_PRICE = {
	Bacon: 0.7,
	Cheese: 0.4,
	Meat: 1.3,
	Salad: 0.5
};

const addIngredient = (state, action) => {
	const updatedIngredient = {
		[action.ingredientName]: state.ingredients[action.ingredientName] + 1
	};
	const updatedIngredients = updateObject(
		state.ingredients,
		updatedIngredient
	);
	const updateState = {
		ingredients: updatedIngredients,
		totalPrice: state.totalPrice + INGREDIENTS_PRICE[action.ingredientName]
	};
	return updateObject(state, updateState);
};

const removeIngredient = (state, action) => {
	const updatedIngr = {
		[action.ingredientName]: state.ingredients[action.ingredientName] - 1
	};
	const updatedIngrs = updateObject(state.ingredients, updatedIngr);
	const updateStateRemove = {
		ingredients: updatedIngrs,
		totalPrice: state.totalPrice - INGREDIENTS_PRICE[action.ingredientName]
	};
	return updateObject(state, updateStateRemove);
};

const setIngredients = (state, action) => {
	return updateObject(state, {
		ingredients: action.ingredients,
		totalPrice: initialState.totalPrice,
		error: false
	});
};

const setIngredientsFail = (state, action) => {
	return updateObject(state, { error: true });
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.ADD_INGREDIENT:
			return addIngredient(state, action);
		case actionTypes.REMOVE_INGREDIENT:
			return removeIngredient(state, action);
		case actionTypes.SET_INGREDIENTS:
			return setIngredients(state, action);
		case actionTypes.SET_INGREDIENTS_FAILED:
			return setIngredientsFail(state, action);
		default:
			return state;
	}
};

export default reducer;
