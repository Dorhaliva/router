import reducer from './auth';
import * as actionTypes from '../actions/actionTypes';

describe('auth reducer', () => {
	it('should return initial state', () => {
		const initialState = {
			idToken: null,
			userId: null,
			error: null,
			loading: false
		};
		expect(reducer(undefined, {})).toEqual(initialState);
	});
});
