import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utillity';

const initialState = {
	orders: [],
	loading: false,
	purchased: false
};

const purchaseInit = state => {
	return updateObject(state, { purchased: false });
};

const purchaseBurgerSuccess = (state, action) => {
	const newOrder = updateObject(action.orderData, action.id);
	return updateObject({
		loading: false,
		orders: state.orders.concat(newOrder),
		purchased: true
	});
};

const purchaseBurgerFail = state => {
	return updateObject(state, { loading: false });
};

const purchaseBurgerStart = state => {
	return updateObject(state, { loading: true });
};
const getOrdersStart = state => {
	return updateObject(state, {
		loading: true
	});
};
const getOrdersSuccess = (state, action) => {
	return updateObject(state, {
		orders: action.orders,
		loading: false
	});
};
const getOrdersFail = state => {
	return updateObject(state, {
		loading: false
	});
};
const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.PURCHASE_INIT:
			return purchaseInit(state);
		case actionTypes.PURCHASE_BURGER_SUCCESS:
			return purchaseBurgerSuccess(state, action);
		case actionTypes.PURCHASE_BURGER_FAIL:
			return purchaseBurgerFail(state);
		case actionTypes.PURCHASE_BURGER_START:
			return purchaseBurgerStart(state);
		case actionTypes.GET_ORDERS_SUCCESS:
			return getOrdersSuccess(state, action);
		case actionTypes.GET_ORDERS_START:
			return getOrdersStart(state);
		case actionTypes.GET_ORDERS_FAIL:
			return getOrdersFail(state);
		default:
			return state;
	}
};

export default reducer;
