import * as actionType from './actionTypes';
import axios from '../../axios-orders';
import { updateObject } from '../../shared/utillity';

export const purcahseBurgerSuccess = (id, orderData) => {
	return {
		type: actionType.PURCHASE_BURGER_SUCCESS,
		id,
		orderData
	};
};

export const purcahseBurgerFail = error => {
	return {
		type: actionType.PURCHASE_BURGER_FAIL,
		error
	};
};

const purcahseBurgerStart = () => {
	return {
		type: actionType.PURCHASE_BURGER_START
	};
};

export const purcahseBurger = (orderData, token) => {
	return dispatch => {
		dispatch(purcahseBurgerStart());
		axios
			.post('/orders.json?auth=' + token, orderData)
			.then(response => {
				dispatch(purcahseBurgerSuccess(response.data.name, orderData));
			})
			.catch(error => {
				dispatch(purcahseBurgerFail(error));
			});
	};
};

export const purchaseInit = () => {
	return {
		type: actionType.PURCHASE_INIT
	};
};

export const getOrders = (token, userId) => {
	return dispatch => {
		dispatch(getOrdersStart());
		axios
			.get(
				`/orders.json?auth=${token}&orderBy="userId"&equalTo="${userId}"`
			)
			.then(res => {
				const orders = [];
				for (let key in res.data) {
					orders.push(
						updateObject(res.data[key], {
							id: key
						})
					);
				}
				dispatch(getOrderSuccess(orders));
			})
			.catch(err => {
				dispatch(getOrderFail(err));
			});
	};
};

const getOrdersStart = () => {
	return {
		type: actionType.GET_ORDERS_START
	};
};

const getOrderSuccess = orders => {
	return {
		type: actionType.GET_ORDERS_SUCCESS,
		orders: orders
	};
};

const getOrderFail = error => {
	return {
		type: actionType.GET_ORDERS_FAIL,
		error
	};
};
