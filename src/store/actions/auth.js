import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
	return {
		type: actionTypes.AUTH_START
	};
};

export const authSuccess = (idToken, userId) => {
	return {
		type: actionTypes.AUTH_SUCCESS,
		idToken,
		userId
	};
};
export const authFail = error => {
	return {
		type: actionTypes.AUTH_FAIL,
		error
	};
};

export const checkAuthTimeout = expirationTime => {
	return dispatch => {
		setTimeout(() => {
			dispatch(logout());
		}, expirationTime * 1000);
	};
};

export const logout = () => {
	window.localStorage.removeItem('token');
	window.localStorage.removeItem('expirationDate');
	window.localStorage.removeItem('userId');
	return {
		type: actionTypes.AUTH_LOGOUT
	};
};

export const authCheckState = () => {
	return dispatch => {
		const token = localStorage.getItem('token');
		if (!token) {
			dispatch(logout());
		} else {
			const expirationDate = new Date(
				localStorage.getItem('expirationDate')
			);
			if (expirationDate < new Date()) {
				dispatch(logout());
			} else {
				dispatch(authSuccess(token, localStorage.getItem('userId')));
				dispatch(
					checkAuthTimeout(
						(expirationDate.getTime() - new Date().getTime()) / 1000
					)
				);
			}
		}
	};
};

export const auth = (email, password, isSignUp) => {
	return dispatch => {
		dispatch(authStart());
		const authData = { email, password, returnSecureToken: true };
		let url =
			'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyATIS5_F9rrfxCBbaas4ZtcsfVe11fk4EM';
		if (!isSignUp)
			url =
				'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyATIS5_F9rrfxCBbaas4ZtcsfVe11fk4EM';
		axios
			.post(url, authData)
			.then(res => {
				const expirationDate = new Date(
					new Date().getTime() + res.data.expiresIn * 1000
				);
				window.localStorage.setItem('token', res.data.idToken);
				window.localStorage.setItem('expirationDate', expirationDate);
				window.localStorage.setItem('userId', res.data.localId);
				dispatch(authSuccess(res.data.idToken, res.data.localId));
				dispatch(checkAuthTimeout(res.data.expiresIn));
			})
			.catch(res => {
				dispatch(authFail(res.response.data.error.message));
			});
	};
};
