import React from 'react';
import classes from './Order.css';

const order = props => {
	let ingredients = null;

	if (props.ingredients) {
		ingredients = Object.keys(props.ingredients).map(igKey => {
			return (
				<span className={classes.Ingredient} key={igKey}>
					{igKey} : {props.ingredients[igKey]}
				</span>
			);
		}, []);
	}
	return (
		<div className={classes.Order}>
			<p>Ingredients: {ingredients}</p>
			<p>
				Price: <strong>{props.price.toFixed(2)}$</strong>
			</p>
		</div>
	);
};

export default order;
