import React, { Fragment } from 'react';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
import Backdrop from '../../UI/Backdrop/Backdrop';

const sideDrawer = props => {
	// ...
	const attachedClasses = [classes.SideDrawer, classes.Close];
	if (props.isOpen) {
		attachedClasses[1] = classes.Open;
	}
	return (
		<Fragment>
			<Backdrop show={props.isOpen} clicked={props.closed} />
			<div className={attachedClasses.join(' ')}>
				<div className={classes.Logo}>
					<Logo />
				</div>
				<nav onClick={props.closed}>
					<NavigationItems isAuth={props.isAuth} />
				</nav>
			</div>
		</Fragment>
	);
};

export default sideDrawer;
