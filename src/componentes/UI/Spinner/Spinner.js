import React from 'react';
import classes from './Spinner.css';

const spinner = () => <div className={classes.Loader}>Loadding...</div>;

export default spinner;
