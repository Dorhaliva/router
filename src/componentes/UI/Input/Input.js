import React from 'react';
import classes from './Input.css';

const input = props => {
	let inputElement = null;
	const inputClasses = [classes.InputElement];

	if (!props.valid && props.shouldValidate) {
		inputClasses.push(classes.Invalid);
	}

	switch (props.elementType) {
		case 'input':
			inputElement = (
				<input
					onChange={props.click}
					className={inputClasses.join(' ')}
					{...props.elementConfig}
					value={props.value}
				/>
			);
			break;
		case 'textarea':
			inputElement = (
				<textarea
					onChange={props.click}
					className={inputClasses.join(' ')}
					{...props.elementConfig}
					value={props.value}
				/>
			);
			break;
		case 'select':
			inputElement = (
				<select
					className={inputClasses.join(' ')}
					value={props.value}
					onChange={props.click}
				>
					{props.elementConfig.options.map(option => (
						<option key={option.displayValue} value={option.value}>
							{option.displayValue}
						</option>
					))}
				</select>
			);
			break;
		default:
			inputElement = (
				<input
					className={inputClasses.join(' ')}
					{...props.elementConfig}
					value={props.value}
				/>
			);
			break;
	}

	return (
		<div className={classes.Input}>
			<label className={classes.Label} label={props.label}></label>
			{inputElement}
		</div>
	);
};

export default input;
