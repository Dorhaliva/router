import React from 'react';
import classes from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
	{
		label: 'Bacon',
		type: 'Bacon'
	},
	{
		label: 'Cheese',
		type: 'Cheese'
	},
	{
		label: 'Meat',
		type: 'Meat'
	},
	{
		label: 'Salad',
		type: 'Salad'
	}
];

const buildControls = props => (
	<div className={classes.BuildControls}>
		{controls.map(c => (
			<BuildControl
				disabled={props.disabled[c.type]}
				moreClick={() => props.add(c.type)}
				lessClick={() => props.remove(c.type)}
				label={c.label}
				key={c.label}
			></BuildControl>
		))}
		<button
			className={classes.OrderButton}
			disabled={!props.purchasable}
			onClick={props.ordered}
		>
			{props.isAuth ? 'Order Now' : 'Sign up to order'}
		</button>
	</div>
);

export default buildControls;
