import React, { Component } from 'react';
import classes from './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

class Burger extends Component {
	createIngredients() {
		return Object.keys(this.props.ingredients).map(iKey => {
			return [...Array(this.props.ingredients[iKey])].map(
				(value, index) => {
					// itarate the array
					return <BurgerIngredient key={iKey + index} type={iKey} />;
				}
			);
		});
	}

	convertIngredientsToList(ingredients) {
		return ingredients.reduce((arr, el) => {
			// join all the array
			return arr.concat(el);
		}, []);
	}

	render() {
		let ingredientsList = this.convertIngredientsToList(
			this.createIngredients()
		);

		if (ingredientsList.length === 0) {
			ingredientsList = <p>Please Start adding ingredients</p>;
		}
		return (
			<div className={classes.Burger}>
				<BurgerIngredient type="BreadTop"></BurgerIngredient>
				{ingredientsList}
				<BurgerIngredient type="BreadBottom"></BurgerIngredient>
			</div>
		);
	}
}

export default Burger;
