import React, { Fragment, Component } from 'react';
import Button from '../../UI/Button/Button';
class OrderSummary extends Component {
	render() {
		const ingredientSummary = Object.keys(this.props.ingredients).map(
			ikey => {
				return (
					<li key={ikey}>
						{ikey}: {this.props.ingredients[ikey]}
					</li>
				);
			}
		);

		return (
			<Fragment>
				<h3>
					Your Oredr
					<p>Delicious burger with the </p>
				</h3>
				<ul>{ingredientSummary}</ul>
				<p>
					<strong>Total Price: {this.props.price.toFixed(2)}</strong>
				</p>
				<p>Continue to checkout</p>
				<Button clicked={this.props.cancel} btnType="Danger">
					CANCEL
				</Button>
				<Button clicked={this.props.continue} btnType="Success">
					CONTINUE
				</Button>
			</Fragment>
		);
	}
}

export default OrderSummary;
