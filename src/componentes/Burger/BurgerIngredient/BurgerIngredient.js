import React from 'react';

import classes from './BurgerIngredient.css';
import PropTypes from 'prop-types';

const burgerIngredient = props => {
	return <div className={classes[props.type]}></div>;
};

burgerIngredient.propTypes = {
	type: PropTypes.string.isRequired
};

export default burgerIngredient;
